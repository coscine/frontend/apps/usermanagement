declare module "*.vue" {
  import Vue from "vue";
  export default Vue;
}

declare module "@coscine/api-connection";
declare module "@coscine/project-creation";
declare module "@coscine/app-util";
declare module "@coscine/component-library";

declare module "*.png" {
  const value: string;
  export default value;
}
