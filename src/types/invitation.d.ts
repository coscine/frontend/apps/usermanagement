declare interface Invitation {
  expiration: string;
  id: string;
  issuer: string;
  projectId: string;
  roleId: string;
  userMail: string;
  role: ?string;
}

export = Invitation;
